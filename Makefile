CC=/home/ferad/apps/mcxx-rsl/bin/mcc
NANOS_ROOT = /home/ferad/apps/nanox-rsl
CFLAGS= -I$(NANOS_ROOT)/include/nanox-dev --ompss --debug -O0
# -g3 --debug -k 


all: MatrixSum MatrixSumMpoison

MatrixSumMpoison: MatrixSumMpoison.c
	$(CC) $(CFLAGS) -o $@ $+
        
MatrixSum: MatrixSum.c
	$(CC) $(CFLAGS) -o $@ $+

RunMatrixSumMpoison: MatrixSumMpoison
	NX_ARGS="--memory-poisoning --mpoison-rate=1 --deps regions --smp-cpus=1 --verbose --summary"  ./MatrixSumMpoison &> MatrixSumMpoison.out

RunMatrixSumMpoison2: MatrixSumMpoison
	NX_ARGS="--memory-poisoning --mpoison-rate=10 --deps regions --smp-cpus=1 --verbose --summary"  ./MatrixSumMpoison
        
RunMatrixSum: MatrixSum
	./MatrixSum


clean:
	rm -Rf MatrixSumMpoison MatrixSumMpoison.o mcc_MatrixSumMpoison.c MatrixSum MatrixSum.o mcc_MatrixSum.c *~