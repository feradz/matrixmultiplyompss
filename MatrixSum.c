/*
 * Example dense matrix multiplication using tasks.
 *
 * Compile with OmpSs
 * $mcc --ompss -g -O0 -o MatrixMultiply MatrixMultiply.c
 *
 * Compile with Gcc
 * $gcc -g -O0 -o MatrixMultiply MatrixMultiply.c
 */

#include <stdio.h>
#include <stdlib.h>

#pragma omp target device(smp) copy_deps
#pragma omp task in( [length] inA, [length] inB ) out( [length] out ) recover
void sum(int *inA, int *inB, int *out, int length)
{
	for (int i = 0; i < length; i++)
	{
		out[i] = inA[i] + inB[i];
	}
}

#pragma omp target device(smp) copy_deps
#pragma omp task inout( [length] a) recover
void initialize(int *a, int length, int value)
{
	for (int i = 0; i < length; i++)
	{
		a[i] = value;
	}
}

char validate(int *a, int *b, int *c, int length)
{
	int temp;

	for (int i = 0; i < length; i++)
	{
		temp = a[i] + b[i];
		if (c[i] != temp)
		{
			fprintf(stderr, "Error. expected %d, actual %d, i=%d\n", temp, c[i], i);
			return 0;
		}
	}

	return 1;
}

int main(int argc, char* argv[])
{
    printf("OmpSs demo matrix multiply.\n");
    int N = 16384;
    int blockSize = 1024;
    int numBlocks = N / blockSize;
    int blockInd = 0;

    int *a = malloc(N * sizeof(int));
    int *b = malloc(N * sizeof(int));
    int *c = malloc(N * sizeof(int));

    int *aIter = a;
    int *bIter = b;
    int *cIter = c;

    for (blockInd = 0; blockInd < numBlocks; blockInd++)
    {
    	initialize(aIter, blockSize, blockInd);
    	initialize(bIter, blockSize, blockInd);

    	aIter += blockSize;
    	bIter += blockSize;
    }

    aIter = a;
    bIter = b;
    for (blockInd = 0; blockInd < numBlocks; blockInd++)
    {
    	sum(aIter, bIter, cIter, blockSize);
    	aIter += blockSize;
    	bIter += blockSize;
    	cIter += blockSize;
    }

	#pragma omp taskwait

    validate(a, b, c, N);

    free(a);
    free(b);
    free(c);

    printf("Done\n");
}

